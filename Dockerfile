# Start from the ruby base image
ARG RUBY_VERSION=3.2.2
FROM ruby:${RUBY_VERSION}

# Fetcher stage
FROM alpine AS fetcher
# Add curl to fetch buildx
RUN apk add curl
ARG BUILDX_VERSION=0.11.2
RUN curl -L \
  --output /docker-buildx \
  "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64"
# Make the downloaded buildx file executable
RUN chmod a+x /docker-buildx

# Get back to the ruby image and copy the buildx file
FROM ruby:${RUBY_VERSION}

# Copy docker-buildx from the fetcher stage
COPY --from=fetcher /docker-buildx /usr/lib/docker/cli-plugins/docker-buildx

# Install necessary packages
RUN apt-get update -qq && \
    apt-get install -yq docker.io && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install the kamal gem
RUN gem install kamal --without document
